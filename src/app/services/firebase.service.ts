import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
// import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AngularFireDatabase, AngularFireList } from "@angular/fire/database";
import { Observable } from "rxjs";
import { query } from "@angular/core/src/render3";
// Authentificate
import { AngularFireAuth } from "@angular/fire/auth";
import { auth } from "firebase";

@Injectable({
  providedIn: "root"
})
export class FireBaseService {
  items: Observable<any>;
  private dbPath = "user";
  private users: AngularFireList<any> = null;
  private estadoUsuario = {}

  public currentUser: any = {};

  constructor(
    private db: AngularFireDatabase,
    private authFire: AngularFireAuth
  ) {
    this.users = db.list(this.dbPath);

    this.authFire.authState.subscribe(response => {
      console.log("Estado del usuario: ", response);
      if (!response){
        this.currentUser = {};
        return;
      }
      this.currentUser.nombre = response.displayName;
      this.currentUser.uid = response.uid;
      this.currentUser.photo = response.photoURL;
      console.log("Este es el current useeeeeer ", this.currentUser.nombre);
      
      localStorage.setItem("nombre", this.currentUser.nombre);
      localStorage.setItem("photo", this.currentUser.photo);
    }); /** Listen all change on authentication*/
  }

  //Las promesas tienen :    resolve --> resuelve , reject ----> falla
  // El metodo push de firebase me ingresa un nuevo usuario a la lista llamada (user), de mi base de datos

  newUser = (user: any): Promise<any> => {
    return new Promise((resolve, reject) => {
      if (this.users.push(user)) {
        resolve("Se ha guardado correctamente");
      } else {
        reject("Ha ocurrido un error");
      }
    });
  };

  getUser = () => {
    this.users = this.db.list(this.dbPath);
    return this.users
      .valueChanges()
      .pipe(map((data: any) => {
         return data;
      }
       ));
  };

  login(platform: string) {
    let provider: any =
      platform === "google"
        ? new auth.GoogleAuthProvider()
        : platform === "facebook"
        ? new auth.FacebookAuthProvider()
        : new auth.TwitterAuthProvider();

    return new Promise((resolve, reject) => {
      this.authFire.auth.signInWithPopup(provider).then(
        correcta => {
          resolve(this.currentUser);
        },
        incorrecta => {
          reject(incorrecta);
        }
      );
    });
  }

  logout(){
    
    this.authFire.auth.signOut();
    this.currentUser = {};
    localStorage.clear();
  }


  isLogged(){
    if(localStorage.getItem("nombre")){
      return true;
    } 
    return false;
  }

}
