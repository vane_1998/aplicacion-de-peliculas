import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class PeliculasService {
  // Mi api que me da movieDb
  private readonly apikey: string = "9303b9f07f1a50036df5c626daa8d2ae";
  // Se repite en todas las url
  private readonly urlMovie: string = "https://api.themoviedb.org/3/movie/";
  private readonly language: string = "&language=es";

  peliculas : any[] = [];
  

  constructor(private httpClient: HttpClient) {}

  getMoviesCine() {
    const URL: string = `${this.urlMovie}upcoming?api_key=${this.apikey}${
      this.language
    }`;
    return this.httpClient.get(URL).pipe(
      map((data: any) => {
        return data.results;
      })
    );
  }

  getMoviesRated() {
    const URL: string = `${this.urlMovie}top_rated?api_key=${this.apikey}${
      this.language
    }`;
    return this.httpClient.get(URL).pipe(
      map((data: any) => {
        return data.results;
      })
    );
  }

  getMoviesPopulars() {
    const URL: string = `${this.urlMovie}popular?api_key=${this.apikey}${
      this.language
    }`;
    return this.httpClient.get(URL).pipe(
      map((data: any) => {
        return data.results;
      })
    );
  }



  getDetails(idMovie : number){
    // https://api.themoviedb.org/3/movie/{movie_id}?api_key=<<api_key>>&language=en-US
      const URL : string = `${this.urlMovie}${idMovie}?api_key=${this.apikey}${this.language}`;
      return this.httpClient.get(URL);
  }


  getOnlyMovie(nombrePelicula : string){

    // https://api.themoviedb.org/3/search/movie?api_key=e1b649ed7afe8f427e63aeca67b713ce&query=dumbo&fbclid=IwAR22YK2EXbrFMXtrzSq4Hf6yp6XV5i7nDyAi_VnYI-qvdfmG7J4MsdCek8s

    const URL : string = `https://api.themoviedb.org/3/search/movie?query=${nombrePelicula}
    &sort_by=popularity.desc&api_key=${ this.apikey }${this.language}
    `
    return this.httpClient.get(URL).pipe(
      map((data: any) =>{
        this.peliculas = data.results;
        return data.results;
      })
    )
  }

 












































  //   getPopulation(){

  //     let url = `${this.urlMovie}/discover/movie?sort_by=popularity.desc&api_key=${this.apikey}&language=es`;

  // // El map solo me muestra laa informacion de lo que me interesa de esa api
  //      return this.httpClient.get(url).pipe(map(data=>{
  //       console.log(data);
  //     }))
  //   }
}
