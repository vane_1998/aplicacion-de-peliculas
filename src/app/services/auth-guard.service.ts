import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { FireBaseService } from './firebase.service';



@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {


    constructor(private firebaseService : FireBaseService,
        private router : Router){

    }

    canActivate(next : ActivatedRouteSnapshot,
        state: RouterStateSnapshot) : boolean{
            if(this.firebaseService.isLogged()) return true;
            this.router.navigate(['login']);
            return false;
        }
}