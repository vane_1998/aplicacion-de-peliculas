import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './components/login/login.component'
import {SignUpComponent} from './components/sign-up/sign-up.component'
import { HomeComponent } from './components/home/home.component';
import { AuthGuardService } from './services/auth-guard.service';
import { PeliculaComponent } from './components/pelicula/pelicula.component';
import { SearchComponent } from './components/search/search.component';

const APP_ROUTES: Routes = [
  { path: 'login', component: LoginComponent },
    { path: 'signUp', component: SignUpComponent },
    { path: 'home', 
    component: HomeComponent,
    canActivate : [AuthGuardService]
  },
  { path: 'pelicula/:id/:page', 
    component: PeliculaComponent,
    canActivate : [AuthGuardService]
  },
  { path: 'pelicula/:id/:page/:busqueda', 
    component: PeliculaComponent,
    canActivate : [AuthGuardService]
  },
  { path: 'search/:texto', component: SearchComponent },
  { path: 'search', 
  component: SearchComponent,
  canActivate : [AuthGuardService]
},
  { path: '**', pathMatch: 'full', redirectTo: 'login' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
