import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';


//Firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';



// formularios por data import
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

// Pipes





import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http'

// Routers
import {APP_ROUTING} from './app.routes';

/*Providers*/
import {PeliculasService} from './services/peliculas.service'
import { HttpModule } from '@angular/http';
import { FireBaseService } from './services/firebase.service';
import { environment } from '../environments/environment';
import { HomeComponent } from './components/home/home.component';
import { ImagenPipePipe } from './pipes/imagen-pipe.pipe';
import { GalleryComponent } from './components/shared/gallery/gallery.component';
import { PeliculaComponent } from './components/pelicula/pelicula.component';
import { SearchComponent } from './components/search/search.component';







@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    SignUpComponent,
    HomeComponent,
    ImagenPipePipe,
    GalleryComponent,
    PeliculaComponent,
    SearchComponent
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase),  ///este modulo va a inicializar firebase que se encuentra en enviroment
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    APP_ROUTING

  ],
  providers: [
    PeliculasService,
    FireBaseService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
