import { Component, OnInit } from '@angular/core';
import { PeliculasService } from 'src/app/services/peliculas.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent implements OnInit {

  buscar : string = "";
  constructor(public movieService : PeliculasService,
    private activatedRoute : ActivatedRoute) {
      this.activatedRoute.params.subscribe(data =>{
        this.movieService.peliculas = [];
        if(data['texto']){
          this.buscar = data['texto'];
          this.searchMovie();
          
        }
        
      })
     }

  ngOnInit() {
  }

  searchMovie(){
    this.movieService.getOnlyMovie(this.buscar).subscribe(data =>{
      console.log(data);
      
    });
  }

}
