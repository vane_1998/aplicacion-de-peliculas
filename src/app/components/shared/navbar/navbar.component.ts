import { Component, OnInit } from '@angular/core';
import { FireBaseService } from 'src/app/services/firebase.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

  constructor(private firebaseService : FireBaseService,
    private router : Router) { }

  ngOnInit() {
  }

  logout(){
    this.firebaseService.logout();
    this.router.navigate(['login']);

  }


  buscarPeli(nombrePeli: string){
    if(nombrePeli.length == 0){
      return;
    }
   this.router.navigate(['search', nombrePeli]);
    
  }

}
