import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PeliculasService } from 'src/app/services/peliculas.service';


@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styles: []
})
export class PeliculaComponent implements OnInit {

  private peliculaId;
  private previousPage;
  private currentMovie : any = {};
  private isLoad : boolean;
  private busqueda: string;

  constructor(private route: ActivatedRoute,
    private movieServices : PeliculasService,
    private router: Router) {
    this.route.params.subscribe(params => {
      this.peliculaId = params.id;
      this.previousPage = params.page;
      if(params['busqueda']){
        this.busqueda = params['busqueda'];
        // tambien se puede poner de esta forma ----->  this.busqueda = params.busqueda;
      }
      this.movieServices.getDetails(this.peliculaId).subscribe((data : any) =>{
        this.currentMovie.photo = data.poster_path;
        this.currentMovie.title = data.title;
        this.currentMovie.description = data.overview;
        this.currentMovie.vote_average = data.vote_average;
        this.currentMovie.popularity = (data.popularity * 10)/data.vote_count;
        this.isLoad = true;
        
      })
     
    })
   }

  ngOnInit() {
  }


  redirect(){

    if(this.previousPage != "home"){
      this.router.navigate([this.previousPage, this.busqueda]);
    }else{
      this.router.navigate([this.previousPage]);
    }
    
  }


}
