import { Component } from "@angular/core";
import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { FireBaseService } from "../../services/firebase.service";
import { Observable } from "rxjs";

// EL formGroup debe existir en el html y en el ts, ademas el formGroup agrupa componentes mas pequenos de un formulario
@Component({
  selector: "app-sign-up",
  templateUrl: "./sign-up.component.html",
  styles: []
})
export class SignUpComponent {
  isSave: boolean = true;

  alert: string = "";
  check : boolean = true;

  modal: any = {
    title : "",
    body  : ""
  };

  usuario: any = {
    name: "Vane",
    user: "VANESSA",
    password1: "111",
    password2: "111",
    email: "nicolegualan@gmail.com"
    
  };

  // formulario del html
  formulario: FormGroup;
  constructor(private service: FireBaseService) {
    this.formulario = new FormGroup({
      user: new FormControl(
        "",
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(10)
        ],
        this.existeUsuario
      ),
      password1: new FormControl("", [
        Validators.required,
        Validators.minLength(5)
      ]),
      password2: new FormControl("", [Validators.required, this.igual]),
      email: new FormControl("", [
        Validators.required,
        Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$")
      ]),
      name: new FormControl(
        "",[
          
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(10)
        ]
      )
    });

    this.formulario.setValue(this.usuario);

    /*EL metodo valueChanges es un observable esto significa que pasa escuchando
    los cambios que existen en el formulario, como es un observable tiene que tener
    el subcribe que me extrae esos valores que han cambiado en el formulario*/
    this.formulario.valueChanges.subscribe(data => {
      this.usuario = data;
    });

    this.formulario.controls["password1"].valueChanges.subscribe(data =>{

      let lastValue = this.formulario.controls["password2"].value;
      this.formulario.controls["password2"].reset();
      this.formulario.controls["password2"].setValue(lastValue);
      this.formulario.controls["password2"].markAsTouched();
    })

    this.formulario.controls["email"].valueChanges.subscribe(data => {
      console.log(this.formulario.controls["email"].errors);
    });

    this.formulario.controls["password2"].valueChanges.subscribe(data => {
      console.log(this.formulario.controls["password2"].errors);
      console.log(this.formulario.controls["user"].errors);
    });

    this.formulario.controls["user"].valueChanges.subscribe(data => {
      console.log(this.formulario.controls["user"]);
    });

    this.formulario.controls["name"].valueChanges.subscribe(data => {
      console.log(this.formulario.controls["name"]);
    });

  }
  /*Retorna una respuesta de tipo clave, valor. La clave va a ser de tipo string y el valor un booleano
ES6 la funcion puede ser con =
La funcion de flecha obvia el this.*/
  igual = (control: FormControl): {[key: string]: boolean }  => {

    if (control.value !== this.usuario.password1) {
      return {
        noIguales: true
      };
    }
    return null;
  };

  // Esta es un validacion asincrona
  // El control value te devuelve una promesa
  // Las validaciones asincronas te devuelven una promesa o un observable
  // Este metodo es un validador y solo se lo va a pone en el user.
  existeUsuario = (control: FormControl): Promise<{ [key: string]: boolean }> | Observable<{ [key: string]: boolean }> => {
   
    return new Promise((resolve, reject)=>{
      this.service.getUser().subscribe(data =>{

        data = data.filter(users => users.user == (this.usuario.user));
        if(data.length > 0){
          resolve({isExist : true});
        }else{
          resolve(null);
        }
      });
    });
  }

  saveUser() {
    /*Mandamos a guardar un nuevo usuario a firebase*/
    // console.log(this.formulario.value);
    if(this.check){

      this.service.newUser(this.usuario).then(
        result => {
          this.modal.title ="Todo correcto";
          this.modal.body= result;
          this.alert = "success";
        },
        error => {
          this.modal.title = "Algo ha ocurrido";
          this.modal.body = error;
          this.alert = "error";
          console.log(error);
        }
      );
      // For isVisible = false to the alert
      setTimeout(() => {
        this.isSave = false;
      }, 4000);
        
    }else{
      this.modal.title = "Algo ha ocurrido";
      this.modal.body = "No ha aceptado los termino y condiciones";
     


    }
   
  }
}
