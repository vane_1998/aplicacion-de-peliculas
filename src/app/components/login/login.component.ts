import { Component } from "@angular/core";
import { FireBaseService } from "src/app/services/firebase.service";
import { Router } from "@angular/router";


@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent {

   public usuarioNormal : boolean;
   

  constructor(private firebaseService : FireBaseService, 
    private router : Router) {
      if(firebaseService.isLogged()){
        this.router.navigate(['home']);
      }
    }

  login(platform: string) {
    this.firebaseService.login(platform).then(correcta =>{
      this.router.navigate(['home']);
     
      
    }, incorrect =>{
      console.log(incorrect);
    });
  }

  loginByUser(userName: string, password: string){
    console.log(userName, password);
    
    this.firebaseService.getUser().subscribe(data =>{
      console.log("Esta es la data que necesitoooo",data);
      
      data.forEach(element => {
        if(element.user === userName && element.password1 === password){
          localStorage.setItem("nombre", userName);
          this.router.navigate(['home']);
          this.usuarioNormal = true;
          console.log(this.usuarioNormal);
          localStorage.setItem("nombre", element.name);
          localStorage.setItem("photo", "assets/feliz.jpg");

        }
        
      });
    })
  }


  

}
