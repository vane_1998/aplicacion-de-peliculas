import { Component, OnInit } from '@angular/core';
import { PeliculasService } from 'src/app/services/peliculas.service';
import { FireBaseService } from 'src/app/services/firebase.service';
import { LoginComponent } from '../login/login.component';





@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent  {

 
  private cartelera = [];
  private rated = [];
  private populars = [];
  private nombreUsuario = "";
  private imgUsuario = "";
  private activo : boolean;
  

  constructor(private movieService : PeliculasService,
    public firebaseService : FireBaseService,
   ) { 
    this.loadBillBoardList();
    this.loadBillBoardListRated();
    this.loadBillBoardListPopulars();
    this.nombreUsuario = localStorage.getItem("nombre");
    this.imgUsuario = localStorage.getItem("photo");
    
  }

  loadBillBoardList(){
    this.movieService.getMoviesCine().subscribe(list =>{
      this.cartelera = list;
      console.log(list);
                
    });
  }

  loadBillBoardListRated(){
    this.movieService.getMoviesRated().subscribe(list =>{
      this.rated = list;
      console.log(list);
                
    });
  }

  loadBillBoardListPopulars(){
    this.movieService.getMoviesPopulars().subscribe(list =>{
      this.populars = list;
      console.log(list);
                
    });
  }





}
