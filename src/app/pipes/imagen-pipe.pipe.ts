import { Pipe, PipeTransform } from '@angular/core';
import{DomSanitizer} from '@angular/platform-browser';



@Pipe({
  name: 'imagenPipe'
})
export class ImagenPipePipe implements PipeTransform {

  url : string = "https://image.tmdb.org/t/p/original";

  constructor(private domSanitizer : DomSanitizer){
    
  }

  transform(value: string): any {
    return `${this.url}${value}`;
  }

}
