// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCnlKIaYTS6q73RQ0Tv7W8MPUJY5jEgi_M",
    authDomain: "peliculasapp-7ea53.firebaseapp.com",
    databaseURL: "https://peliculasapp-7ea53.firebaseio.com",
    projectId: "peliculasapp-7ea53",
    storageBucket: "peliculasapp-7ea53.appspot.com",
    messagingSenderId: "7502060847"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
